import React, {Component} from 'react';
import axios from 'axios';
import { Link } from 'react-router';
import MyGlobleSetting from './MyGlobleSetting';


/**
 * Update the specified user 
 * 
 * Specified by user Id
 */

class UpdateUser extends Component{
    constructor(props){
        super(props);
        this.state = {name:'', email:'', number:'', message:''};

        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePhone = this.handlePhone.bind(this);
        this.handleMessage = this.handleMessage.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount(){
        axios.get(MyGlobleSetting.url + '/api/user/${this.props.params.id} / edit')
        .then(responce => {
            this.setState({name: responce.data.name,
                        email: responce.data.email,
                        number: responce.data.number,
                        message: responce.data.message});
        })
        .catch(function(error){
            console.log(error);
        })
    }

    handleName(e){
        this.setState({
            name:e.target.value
        })
    }

    handleEmail(e){
        this.setState({
            email:e.target.value
        })
    }

    handlePhone(e){
        this.setState({
            number:e.target.value
        })
    }

    handleMessage(e){
        this.setState({
            message:e.target.value
        })
    }

    handleSubmit(event){
        event.preventDefault();
        const user ={
            name: this.state.name,
            email: this.state.email,
            number: this.state.number,
            message: this.state.message
        }


        let uri = MyGlobleSetting.url + '/api/user/' + this.props.params.id;
        axios.patch(uri, user).then((Response)=> {this.props.history.push('/display-user');
        });
    }

    /**
     * 
     * @returns UI class for Update user template
     */

    render(){
        return (
          <div>
            <h1>Update User</h1>
            <div className="row">
              <div className="col-md-10"></div>
              <div className="col-md-2">
                <Link to="/display-user" className="btn btn-success">Return to User</Link>
              </div>
            </div>
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label>User Name </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.name}
                      onChange={this.handleName} />
                </div>

                <div className="form-group">
                    <label>User Email </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.email}
                      onChange={this.handleEmail} />
                </div>

                <div className="form-group">
                    <label>User Phone </label>
                    <input type="text"
                      className="form-control"
                      value={this.state.number}
                      onChange={this.handlePhone} />
                </div>
    
    
                <div className="form-group">
                    <label name="product_body">Message</label>
                    <textarea className="form-control"
                      onChange={this.handleMessage} value={this.state.message}></textarea>  
                </div>
    
    
                <div className="form-group">
                    <button className="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
        )
    }   
}

export default UpdateUser;