import axios from 'axios';
import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import MyGlobleSetting from './MyGlobleSetting';

/**
 * Submit User data.
 * 
 * handle user data
 * 
 */

class CreateUser extends Component{
    constructor(props){
        super(props);

        this.state = {userName:'', userEmail:'', userPhone:'', Message:''};

        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePhone = this.handlePhone.bind(this);
        this.handleMessage = this.handleMessage.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleName(e){
        this.setState({
            userName:e.target.value
        })
    }

    handleEmail(e){
        this.setState({
            userEmail:e.target.value
        })
    }

    handlePhone(e){
        this.setState({
            userPhone:e.target.value
        })
    }

    handleMessage(e){
        this.setState({
            Message:e.target.value
        })
    }

    handleSubmit(e){
        e.preventDefault();
        const user ={
            name: this.state.userName,
            email: this.state.userEmail,
            number: this.state.userPhone,
            message: this.state.Message
        }


        let uri = MyGlobleSetting.url + '/api/user';
        axios.post(uri, user).then((Response)=> {browserHistory.push('/display-user');
        });
    }

    /**
     * 
     * @returns User Data create Template UI
     * 
     */

    render(){
        return(
            <div>
                <h1>
                    Add User
                </h1>
                <form onSubmit={this.handleSubmit}>
                    <div className = "row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label> User Name </label>
                                <input type="text" className="form-control" onChange={this.handleName}/>
                            </div>
                        </div>
                    </div>

                    <div className = "row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label> User Emai </label>
                                <input type="text" className="form-control" onChange={this.handleEmail}/>
                            </div>
                        </div>
                    </div>

                    <div className = "row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label> User Phone </label>
                                <input type="text" className="form-control" onChange={this.handlePhone}/>
                            </div>
                        </div>
                    </div>

                    <div className = "row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label> Messages </label>
                                <input type="text" className="form-control" onChange={this.handleMessage}/>
                            </div>
                        </div>
                    </div><br/>
                    <div className="form-group">
                        <button className="btn btn-primary">Add User</button>
                    </div>
                
                </form>
            </div>
        )
    }

}
export default CreateUser;