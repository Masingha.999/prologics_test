/**
 * Main (Base ) URL for the development mode run.
 */

class MyGlobleSetting{
    constructor(){
        this.url = 'http://localhost:8000';
    }
}

export default (new MyGlobleSetting);