import React, {Component } from 'react';
import ReactDOM from 'react-dom';

import { Router, Route, Link } from 'react-router';

class Example extends Component{
    render(){
        return(
            <div className="container">
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                         <div className="navbar-header">
                            <a className="navbar-brand" > User Management  </a>
                         </div>
                        <ul className="nav navbar-nav">
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="add-item"> Add User</Link></li>
                            <li><Link to="display-item"> All User </Link></li>
                        </ul>
                    </div>
                </nav>
                <div>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Example;

// function Example() {
//     return (
//         <div className="container">
//             <div className="row justify-content-center">
//                 <div className="col-md-8">
//                     <div className="card">
//                         <div className="card-header">Example Component</div>

//                         <div className="card-body">I'm an example component!</div>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     );
// }

// export default Example;

// if (document.getElementById('example')) {
//     ReactDOM.render(<Example />, document.getElementById('example'));
// }
