import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import MyGlobleSetting from './MyGlobleSetting';

/**
 * All users in database view on the table.
 * 
 * Table rows indicate
 */

class TableRow extends Component{
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit (event){
        event.preventDefault();
        let uri = MyGlobleSetting.url + '/api/user/${this.props.obj.id}';
        axios.delete(uri);
        browserHistory.push('/display-user');
    }

    render(){
        return(
        <tr>
            <td>
                {this.props.obj.id}
            </td>
            <td>
                {this.props.obj.name}
            </td>
            <td>
                {this.props.obj.email}
            </td>
            <td>
                {this.props.obj.number}
            </td>
            <td>
                {this.props.obj.message}
            </td>
            <td>
            <form onSubmit={this.handleSubmit}>
                <Link to={"edit/"+this.props.obj.id} className="btn btn-primary">Edit User </Link>
                <input type="submit" value="Delete" className="btn btn-danger"/>
            </form>
            </td>
        </tr>
        );
    }
}

export default TableRow;