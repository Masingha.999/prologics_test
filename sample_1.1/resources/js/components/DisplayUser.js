import React, {Component} from 'react';
import axios from 'axios';
import { Link } from 'react-router';
import TableRow from './TableRow';
import MyGlobleSetting from './MyGlobleSetting';

/**
 * Display all data from the databse
 * 
 * Display table rows. and indicate what actions 
 */

class DisplayUser extends Component{
    constructor (props){
        super(props);
        this.state = {value: '', user: ''};
    }

    componentDidMount(){
        axios.get(MyGlobleSetting.url + '/api/user').then(response => {
            this.setState({user: response.data});
        }).catch(function(error){
            console.log(error);
        })
    }

    tabRow(){
        if(this.state.user instanceof Array){
            return this.state.user.map(function(object, i){
                return;
            })
        }
    }

    /**
     * 
     * @returns Table View tamplate
     * 
     * indicate Actions
     */

    render(){
        return(
            <div>
                <h1> Users </h1>

                <div className="roe">
                    <div className="col-md-10"></div>
                    <div className="col-md-2">
                        <Link to ="/add-user"> Add User </Link>
                    </div>    
                </div><br />

                <table className="table table-hover">
                    <thead>
                        <tr>
                            <td> ID </td>
                            <td>userName</td>
                            <td>userEmail</td>
                            <td>userPhone</td>
                            <td>Message</td>
                            <td width="200px"> Actions </td>
                        </tr>
                    </thead>
                    <tbody> 
                        {this.tabRow() }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default DisplayUser;