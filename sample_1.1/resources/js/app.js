/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//require('./components/Example');


/**
 * 
 */

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import Example from './components/Example';
import CreateUser from './components/CreateUser';
import DisplayUser from './components/DisplayUser';
import UpdateUser from './components/UpdateUser';

render(
    <Router history={browserHistory}>
        <Route path="/" component={Example}>
        <Route path="/ add-user" component={CreateUser}/>
        <Route path="/ display-user" component={DisplayUser}/> 
        <Route path="/ edit-user" component={UpdateUser}/>
        </Route>
    </Router>,
document.getElementById('crud-app')
);


