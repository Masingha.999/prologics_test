<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * display all Users in database
     * 
     * return responce
     */

    public function index(){
        $user = User::all();
        return responce()-> json($user);
    }

    /**
     * store a new user (create new user)
     * 
     * return responce when sucessfully created
     */

    public function store (Request $request){
        user = new User([
            'name' => $request ->get('name'),
            'email'=> $request ->get('email'), 
            'number'=> $request ->get('number'),
            'message'=> $request ->get('message')
        ]);
        $user ->save();

        return responce() -> json('User add sucessfully');
    }

    /**
     * show the specified user for the editing
     * 
     * return responce
     */

    public function edit($id){
        $user = User::find($id);
        return responce()->json ($user);
    }

    /**
     * update the specified user
     * 
     * return responce when sucessfully updated
     */

    public function update (Request $request, $id){
        $user = User::find($id);
        $user-> 'name' => $request ->get('name');
        $user->'email'=> $request ->get('email'); 
        $user->'number'=> $request ->get('number');
        $user->save();

        return responce()->json('User Updated Sucessfully');
    }

    /**
     * delete the specified user in the database
     * 
     * return responce when suceedully deleted
     */

    public function destroy($id){
        $user = User::find($id);
        $user->delete();

        return responce() ->json('User Deleted Sucessfully');
    }
}
